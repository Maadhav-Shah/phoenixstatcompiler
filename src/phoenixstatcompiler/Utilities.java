package phoenixstatcompiler;

////////////////////////////////////////////////////////////////////////////////
//
//  COGNIMEM TECHNOLOGIES INCORPORATED
//  Copyright 2011 Cognimem Technologies Incorporated
//  All Rights Reserved.
//
//  V1KU JAVA API    
//
//  NOTICE: Cognimem permits you to use, modify, and distribute this file
//  in accordance with the terms of the license agreement accompanying it.
//
////////////////////////////////////////////////////////////////////////////////

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsEnvironment;
import java.awt.Image;
import java.awt.MediaTracker;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.IndexColorModel;
import java.awt.image.MemoryImageSource;
import java.awt.image.PixelGrabber;
import java.util.ArrayList;
import javax.imageio.ImageIO;



/**
 * Commonly used functions 
 */
public class Utilities extends Component
{
    private static IndexColorModel _indexColorModel = null;
    private MediaTracker _mediaTracker;
    private Image _image;

    
    
    private Utilities(Image image)
    {
        super();
        
        _mediaTracker = new MediaTracker(this);
        _mediaTracker.addImage(image, 0);
        
        _image = image;
    }
    
    
    
    public static int abs(int value)
    {
        return (value < 0 ? -(value) : value);
    }

    private BufferedImage convert()
    {
        try
        {
            this._mediaTracker.waitForID(0);
        }
        catch(InterruptedException e)
        {

        }

        GraphicsConfiguration graphicsConfig = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDefaultConfiguration();
        BufferedImage bimage = graphicsConfig.createCompatibleImage(this._image.getWidth(null),this._image.getHeight(null));

        
        Graphics g = bimage.getGraphics();
        g.drawImage(_image, 0, 0, null);
        
        return bimage;
    }

    private static byte[] convertIntToByteArray(int value)
    {
        byte[] bytes = new byte[4];
       
        
        bytes[3] =(byte)(value >> 24);
        bytes[2] =(byte)(value >> 16);
        bytes[1] =(byte)(value >>  8);
        bytes[0] =(byte)(value >>  0);
        
        return bytes;
    }

    private static int convertByteArrayToInt(byte[] bytes)
    {
        return (bytes[0] << 24) | (bytes[1] << 16) | (bytes[2] << 8) | bytes[3];
    }

    /**
     * Converts an in memory bitmap integer array to an equivalent byte array
     * @param integers
     * @param bands
     * @return
     */
    public static byte[] convertIntArrayToByteArray(int[] integers, int bands, boolean convertTo256Color)
    {
        byte[] result = null;
        int index = 0;
        
        
        if (convertTo256Color == true)
        {
            result = new byte[integers.length];
        }
        else
        {
            result = new byte[integers.length * bands];
        }
        
        try
        {
            for (index = 0; index < integers.length; index++)
            {
                byte[] b = convertIntToByteArray(integers[index]);

                if (convertTo256Color == true)
                {
                    //average the RGB color bands to derive at a greyscale equivalent
                    result[index] = (byte)((int)((int)((b[0] & 0xff)) + ((int)(b[1] & 0xff)) + ((int)(b[2] & 0xff))) / 3);
                }
                else
                {
                    for (int x = 0; x < bands; x++)
                    {
                        //copy the pixels byte by byte, no conversion are done
                        result[((index * bands) + x)] = (byte)(b[x] & 0xff);
                    }
                }
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
        
        return result;
    }

    /**
     * Converts an in memory bitmap byte array to an equivalent integer array
     * @param bytes
     * @return
     */
    public static int[] convertByteArrayToIntArray(byte[] bytes)
    {
        ArrayList<Integer> integers = new ArrayList<Integer>();
        
        for (int index = 0; index < (bytes.length ); index += 4)
        {
            byte[] fourBytes = new byte[4];
            
            fourBytes[0] = bytes[index];
            fourBytes[1] = bytes[index+1];
            fourBytes[2] = bytes[index+2];
            fourBytes[3] = bytes[index+3];
            
            int integer = convertByteArrayToInt(fourBytes);
            
            integers.add(new Integer(integer));
        }
        
        int[] ints = new int[bytes.length/4];
        
        for (int index = 0; index < integers.size() ; index++)
        {
            ints[index] = ((Integer)integers.get(index)).intValue();
        }
        
        return ints;
    }

    /**
     * Converts an in memory bitmap image object to an equivalent byte array
     * @param image
     * @param bands (Number of bands within the image
     * @param convertTo256Color
     * @return
     */
    public static byte[] convertImageToByteArray(Image image, int bands, boolean convertTo256Color)
    {
       
        byte[] result = new byte[0];
        int x = 0;
        int y = 0;
        int w = image.getWidth(null);
        int h = image.getHeight(null);

        
        try
        {
            PixelGrabber pg = new PixelGrabber(image, x, y, w, h, false);
            pg.grabPixels();
            
            Object pixels = pg.getPixels();
            
            if (pixels instanceof byte[])
            {
                result = (byte[]) pixels;
            }
            else if (pixels instanceof int[])
            {
                result = Utilities.convertIntArrayToByteArray((int[])pixels, bands, convertTo256Color);
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }

        return result;
    }

    
    /**
     * Converts an in memory bitmap byte array to an equivalent image object
     * @param vector
     * @param width
     * @param height
     * @return
     */
    public static Image convertByteArrayToImage(byte[] vector, int width, int height)
    {
        Image result = null;
        Integer roiFeature = 0;
        

        if (roiFeature == 0)
        {
            MemoryImageSource memoryImageSource = new MemoryImageSource(width, height, getRgbColorModel(), vector, 0, width);

            result = Toolkit.getDefaultToolkit().createImage(memoryImageSource);
        } 
        else if (roiFeature == 1)
        {
            // TODO: 
            MemoryImageSource memoryImageSource = new MemoryImageSource(width, height, getRgbColorModel(), vector, 0, width);

            result = Toolkit.getDefaultToolkit().createImage(memoryImageSource);
        } 
        else if (roiFeature == 2)
        {
            // TODO: 
            MemoryImageSource memoryImageSource = new MemoryImageSource(width, height, getRgbColorModel(), vector, 0, width);

            result = Toolkit.getDefaultToolkit().createImage(memoryImageSource);
        }

        return result;
    }
    
    /**
     * Reads a standard RGB color model
     * @return
     */
    public static ColorModel getRgbColorModel()
    {
        if (_indexColorModel == null)
        {
            byte[] rgb = new byte[256];

            for (int i = 0; i < rgb.length; i++)
            {
                rgb[i] = (byte) i;
            }

            _indexColorModel = new IndexColorModel(8, 256, rgb, rgb, rgb);
        }

        return _indexColorModel;
    }
    
    /**
     * Convert a Color object to its string equivalent
     * @param value
     * @return
     */
    public static String convertColorToString(Color value)
    {
        String result = String.valueOf(value.getRGB());
     
        
        return result;
    }
       
    /**
     * Convert a string value of a color to its Color object equivalent
     * @param value
     * @return
     */
    public static Color convertStringToColor(String value)
    {
        int intColor = Integer.valueOf(value);
        Color result = new Color(intColor);
     
        
        return result;
    }
    
    /**
     * Creates a blank image object based on the size and color parameters passed in
     * @param width
     * @param height
     * @param color
     * @return
     */
    public static Image createBlankImage(int width, int height, Color color)
    { 
        BufferedImage bi = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        
        Graphics g = bi.getGraphics();
        
        g.setColor(color);
        g.fillRect(0, 0, width, height);
        
        Image result = convertBufferedImageToImage(bi);
                
        return result;
    }
    
    /**
     * Converts a buffered image object to an image object
     * @param value
     * @return
     */
    public static Image convertBufferedImageToImage(BufferedImage value)
    {
        //Image result = Toolkit.getDefaultToolkit().createImage(value.getSource());

        return (Image) value;
    }
    
    /**
     * Converts an image object to a buffered image object
     * @param image
     * @param imageType
     * @return
     */
    public static BufferedImage convertImageToBufferedImage(Image image, int imageType)
    {
        // BufferedImage.TYPE_INT_RGB
        // BufferedImage.TYPE_INT_ARGB
        BufferedImage result = new BufferedImage(image.getWidth(null), image.getHeight(null), imageType);
                
        
        result.createGraphics().drawImage(image, null, null);
                
        return result;
    }
    
    /**
     * Creates a blank buffered image object based on the size and color parameters passed in
     * @param width
     * @param height
     * @param color
     * @param imageType
     * @return
     */
    public static BufferedImage createBlankBufferedImage(int width, int height, Color color, int imageType)
    {
        return convertImageToBufferedImage(createBlankImage(width, height, color), imageType);
    }
    
    public static boolean saveImage(Image image, java.io.File imageFile, String format) 
    {
        boolean result=true;
        
        
        try
        {
            ImageIO.write(Utilities.convertImageToBufferedImage(image, BufferedImage.TYPE_INT_RGB), format.toUpperCase(), imageFile);
        }
        catch(Exception e)
        {
            result=false;
        }
        
        return (result);
    }
    
    public static String convertByteArrayToHexString(byte[] value)
    {
        String result = "";
        
        
        for (int x = 0; x < value.length; x++)
        {
            int valueInt = (int)value[x] & 0xFF;
            String valueString = Integer.toHexString(valueInt);

            while (valueString.length() < 2)
            {
                valueString = "0" + valueString;
            }

            if (result.length() == 0)
            {
                result += valueString.toUpperCase();
            }
            else
            {
                result += " " + valueString.toUpperCase();
            }
        }
        
        return result;
    }
    
    public static BufferedImage getScaledInstance(Image sourceImage, int targetWidth, int targetHeight)
    {
        BufferedImage result = new BufferedImage(targetWidth, targetHeight, BufferedImage.TYPE_INT_RGB);
        Graphics2D g = result.createGraphics();

        
        g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC);
        g.drawImage(sourceImage, 0, 0, targetWidth, targetHeight, null);
        g.dispose();

        return result;
    }
}