/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phoenixstatcompiler;

/**
 *
 * @author mkshah
 */
public class Constants {
    
    public static final int objectsInStack = 6;
    public static final int teamsInMatch = 6;
    public static final int totesInCoop = 4;
    
    public Constants()
    {
        
    }
    
}
