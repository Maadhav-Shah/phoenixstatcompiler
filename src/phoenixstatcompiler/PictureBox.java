package phoenixstatcompiler;

////////////////////////////////////////////////////////////////////////////////
//
//  COGNIMEM TECHNOLOGIES INCORPORATED
//  Copyright 2011 Cognimem Technologies Incorporated
//  All Rights Reserved.
//
//  V1KU JAVA API    
//
//  NOTICE: Cognimem permits you to use, modify, and distribute this file
//  in accordance with the terms of the license agreement accompanying it.
//
////////////////////////////////////////////////////////////////////////////////

import java.awt.*;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.image.*;
import java.io.InputStream;
import javax.accessibility.Accessible;
import javax.imageio.ImageIO;
import javax.swing.JPanel;


/**
 * 
 * @author bnagel
 */
public class PictureBox extends JPanel implements Accessible
{
    private Image _originalImage;
    private Image _backgroundImage;
    
    private Graphics2D _backgroundGraphics;
    private Graphics2D _originalGraphics;
    
    private Color _borderColor = new Color(0, 0, 0);    
    private int _borderWidth = 1;
    
    
    /**
     * Reads the scaled factor. This is calculated by dividing the actual image size by the displayed image size
     * If the screen image size if half the size of the original bitmap, the scale factor is 2.
     * @return
     */
    public double getScaleFactor()
    {
        if (_backgroundImage == null || _originalImage == null)
        {
            return 1;
        }
        else
        {
            double result = ((double) _originalImage.getWidth(null)) / ((double) _backgroundImage.getWidth(null));
            
            result = (result == 0 ? 1 : result);
            
            return result;
        }
    }
    
        
    private int _widthImage;
    /**
     * 
     * @return
     */
    public int getImageWidth()
    {
        return _widthImage;
    }
    /**
     * 
     * @param value
     */
    public void setImageWidth(int value)
    {
        _widthImage = value;
    }
    
    private int _heightImage;
    
    /**
     * Reads the height dimension for this control
     * @return
     */
    public int getImageHeight()
    {
        return _heightImage;
    }
    /**
     * Writes the height dimension for this control
     * @param value
     */
    public void setHeight(int value)
    {
        _heightImage = value;
    }    
    
    private Color _backGroundColor;
    
    /**
     * Reads the background color used in the control
     * @return
     */
    public Color getBackGroundColor()
    {
        return _backGroundColor;
    }
    
    /**
     * Writes the background color used in the control
     * @param value
     */
    public void setBackGroundColor(Color value)
    {
        _backGroundColor = value;
    }
    
    private Color _foreGroundColor;
    
    /**
     * Reads the foreground color used in the control
     * @return
     */
    public Color getForeGroundColor()
    {
        return _foreGroundColor;
    }
    
    /**
     * Writes the foreground color used in the control
     * @param value
     */
    public void setForeGroundColor(Color value)
    {
        _foreGroundColor = value;
    }
    
    

    /**
     * Constructor
     * @param imageWidth
     * Sets the image width dimension
     * @param imageHeight
     * Sets the image height dimension
     * @param borderColor
     * Sets the border color for this control
     * @param borderWidth
     * Sets the border width for this control
     */
//    public PictureBox(int viewerwidth, int viewerHeight, int imageWidth, int imageHeight, Color borderColor, int borderWidth) 
    public PictureBox(int width, int height, Color borderColor, int borderWidth) 
    {
        _borderColor = borderColor;
        _borderWidth = borderWidth;
        
        
        if (borderColor != null)
        {
            this.setBorder(javax.swing.BorderFactory.createLineBorder(borderColor, borderWidth));
        }
        
        _widthImage = width;
        _heightImage = height;
               
        _backgroundImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);   
        _backgroundGraphics = (Graphics2D)_backgroundImage.getGraphics();
        
        _originalImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);   
        _originalGraphics = (Graphics2D)_originalImage.getGraphics();
        
        enableEvents(AWTEvent.MOUSE_EVENT_MASK | AWTEvent.MOUSE_MOTION_EVENT_MASK);
        
        addComponentListener(new ComponentListener() 
        {
            @Override
            public void componentResized(ComponentEvent e)
            {
                int w = getWidth();
                int h = getHeight();
                
                if (w > 0 && h > 0)
                {
                    _backgroundImage = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);   
                    _backgroundGraphics = (Graphics2D)_backgroundImage.getGraphics();
                    
                    _backgroundGraphics.drawImage(_originalImage, 0, 0, _backgroundImage.getWidth(null), _backgroundImage.getHeight(null), null);
                    
                    repaint();
                }
            }

            @Override
            public void componentMoved(ComponentEvent e) { }

            @Override
            public void componentShown(ComponentEvent e) { }

            @Override
            public void componentHidden(ComponentEvent e) { }
        });
    }
    
    /**
     * Clears the current image pallette
     */
    public void clear()
    {
        _backgroundGraphics.setColor(_backGroundColor);
        
        _backgroundGraphics.clearRect(0, 0, _widthImage, _heightImage);
        _backgroundGraphics.fillRect(0, 0, _widthImage, _heightImage);
        
                
        repaint();
    }
      
    /**
     * Draws a rectangle as an overlay to the current image
     * @param rectangle
     * @param rectangleLabel
     * Prints this text centered above the rectangle as a label
     * @param color 
     * Used as the foreground property for both the rectangle and the label
     */
    public void drawAnnotation(Rectangle rectangle, String rectangleLabel, Color color)
    {
        drawAnnotation(rectangle, rectangleLabel, color, false);
    }
    
            
    /**
     * Draws a rectangle as an overlay to the current image
     * @param rectangle
     * @param rectangleLabel
     * Prints this text centered above the rectangle as a label
     * @param color 
     * Used as the foreground property for both the rectangle and the label
     * @param appendAnnotation
     * Controls whether existing annotations are erased before adding the annotations passed in
     */
    public void drawAnnotation(Rectangle rectangle, String rectangleLabel, Color color, boolean appendAnnotation)
    {
        double scale = this.getScaleFactor();
        Rectangle scaledRect = new Rectangle((int)(rectangle.x / scale), (int)(rectangle.y / scale), (int)(rectangle.width / scale), (int)(rectangle.height / scale));
        
       
        
        if (appendAnnotation == false)
        {
            _backgroundGraphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            _backgroundGraphics.drawImage(_originalImage, 0, 0, _backgroundImage.getWidth(null), _backgroundImage.getHeight(null), this);
        }
        
        //draws the outer and inner borders
        if (scale != 1)
        {
            _backgroundGraphics.setColor(color);       
            _backgroundGraphics.drawRect(scaledRect.x, scaledRect.y, scaledRect.width, scaledRect.height);
        }
        else
        {
            _backgroundGraphics.setColor(color);       
            _backgroundGraphics.drawRect(scaledRect.x + 1, scaledRect.y + 1, scaledRect.width - 2, scaledRect.height - 2);
            _backgroundGraphics.drawRect(scaledRect.x - 1, scaledRect.y - 1, scaledRect.width + 2, scaledRect.height + 2);

            //draws the white inner box
            _backgroundGraphics.setColor(Color.white);
            _backgroundGraphics.drawRect(scaledRect.x, scaledRect.y, scaledRect.width, scaledRect.height);
        }
        
        if (rectangleLabel.length() > 0)
        {
            FontMetrics fm = _backgroundGraphics.getFontMetrics(_backgroundGraphics.getFont());
            java.awt.geom.Rectangle2D rect = fm.getStringBounds(rectangleLabel, _backgroundGraphics);


            // Center text horizontally and vertically
            int x = scaledRect.x - ((int)(rect.getWidth()) / 2) + (scaledRect.width / 2);
            int y = scaledRect.y - ((int)(rect.getHeight()) / 2);
            
            int minY = 14;
            int maxY = (_heightImage - 8);

            y = (y < minY ? minY : y);
            y = (y > maxY ? maxY : y);
            

            _backgroundGraphics.setColor(Color.white);
            _backgroundGraphics.fillRoundRect(x - 1, y - 11, (int)rect.getWidth() + 1, (int)rect.getHeight() + 1, 4, 4);

            _backgroundGraphics.setColor(color);
            _backgroundGraphics.drawString(rectangleLabel, x, y );
        }
        
        repaint();
    }
    
    /**
     * Prints a string of characters as an overlay to the current image at the X and Y location passed in
     * @param x
     * @param y
     * @param text 
     * @param textColor  
     */
    public void drawAnnotation(int x, int y, String text, Color textColor)
    {
        _backgroundGraphics.setColor(textColor);
        
        _backgroundGraphics.drawString(text, x, y);
        
        repaint();
    }
    
    public void drawCrossHairs()
    {
        _backgroundGraphics.setColor(new Color(224, 112, 40));
        
        
        _backgroundGraphics.drawLine((this.getWidth() / 2), 0, (this.getWidth() / 2), this.getHeight());
        _backgroundGraphics.drawLine(0, (this.getHeight() / 2), this.getWidth(), (this.getHeight() / 2));
        
        repaint();
    }
    
    public void drawHistogram(byte[] vectorBmp)
    {
        _originalImage = new BufferedImage(_widthImage, _heightImage, BufferedImage.TYPE_INT_RGB);   
        _originalGraphics = (Graphics2D)_originalImage.getGraphics();
        
        _originalGraphics.setColor(_foreGroundColor);
        
        double scale = (double)256 / (double)_heightImage;
        
        for (int i = 0; i < vectorBmp.length; i++)   
        {
            double scaledPixel = ((double)(vectorBmp[i] & 0xFF) / (double)scale);
            double calibratedPixel = _heightImage - (scaledPixel - 2);
            
            _originalGraphics.drawLine(i, _heightImage, i, (int)calibratedPixel);
        }
        
        for (int i = 1; i < 5; i++)   
        {
            _originalGraphics.setColor(new Color(224, 112, 40));
            
            _originalGraphics.drawLine(0, (_heightImage / 5) * i, _widthImage, (_heightImage / 5) * i);
        }

        _backgroundGraphics.drawImage(_originalImage, 0, 0, this.getWidth(), this.getHeight(), this);
        
        repaint();
    }
    
    /**
     * Writes an image from the picture box control to the disk file object passed in
     * @param imageFile
     * @return
     */
    public boolean saveImage(java.io.File imageFile) 
    {
        boolean result=true;
        
        
        try
        {
            //ImageIO.write(Utilities.convertImageToBufferedImage(_originalImage, BufferedImage.TYPE_INT_RGB), "PNG", imageFile);
            
            Utilities.saveImage(_originalImage, imageFile, "PNG");
        }
        catch(Exception e)
        {
            result=false;
        }
        
        return (result);
    }
    
    public boolean saveImageWithAnnotations(java.io.File imageFile) 
    {
        boolean result=true;
        
        
        try
        {
            ImageIO.write(Utilities.convertImageToBufferedImage(_backgroundImage, BufferedImage.TYPE_INT_RGB), "PNG", imageFile);
        }
        catch(Exception e)
        {
            result=false;
        }
        
        return (result);
    }
    
    /**
     * Reads an image from the picture box control
     * @return
     */
    public Image getImage()
    {
        return _originalImage;
    }

    /**
     * Writes an image to the picture box control
     * @param image
     * An in memory instance of an Image object
     */
    public void setImage(Image image)
    {
        if (image == null)
        {
            image = Utilities.createBlankImage(100, 100, _backGroundColor);
        }
        
        //this.setBorder(javax.swing.BorderFactory.createLineBorder(_borderColor, _borderWidth));
        
        _originalImage = new BufferedImage(image.getWidth(null), image.getHeight(null), BufferedImage.TYPE_INT_RGB);   

        _originalGraphics = (Graphics2D)_originalImage.getGraphics();

        _originalGraphics.drawImage(image, 0, 0, this);

        _backgroundGraphics.drawImage(_originalImage, 0, 0, _backgroundImage.getWidth(null), _backgroundImage.getHeight(null), this);
        
        repaint();
    }
    
    /**
     * Writes an image to the picture box control
     * @param image
     * An in memory instance of a BufferedImage object
     */
    public void setImage(InputStream imageInputStream)
    {
        //this.setBorder(javax.swing.BorderFactory.createLineBorder(_borderColor, _borderWidth));
        
        if (imageInputStream != null)
        {
            try
            {
                _originalImage = ImageIO.read(imageInputStream);

                _originalGraphics = (Graphics2D)_originalImage.getGraphics();
                
                _backgroundGraphics.drawImage(_originalImage, 0, 0, _backgroundImage.getWidth(null), _backgroundImage.getHeight(null), this);

                repaint();
            }
            catch (Exception ex)
            {
                ex.printStackTrace();
            }
        }   
    }
    
    /**
     * Writes an image to the picture box control
     * @param image
     * An in memory instance of a BufferedImage object
     */
    public void setImage(BufferedImage image)
    {
        //this.setBorder(javax.swing.BorderFactory.createLineBorder(_borderColor, _borderWidth));
        
        if (image == null)
        {
            image = Utilities.createBlankBufferedImage(100, 100, Color.BLACK, BufferedImage.TYPE_INT_RGB);
        }
        
        _originalImage = new BufferedImage(image.getWidth(null), image.getHeight(null), BufferedImage.TYPE_INT_RGB);   
        
        _originalGraphics = (Graphics2D)_originalImage.getGraphics();
        
        _originalGraphics.drawImage(image, 0, 0, this);
            
        _backgroundGraphics.drawImage(image, 0, 0, _backgroundImage.getWidth(null), _backgroundImage.getHeight(null), this);
        
        repaint();
    }
    
    /**
     * Writes an image to the picture box control
     * @param imageFile
     * The file object representing an image stored in disk
     * @return
     */
    public boolean setImage(java.io.File imageFile) 
    {
        boolean result = false;
        
        //this.setBorder(javax.swing.BorderFactory.createLineBorder(_borderColor, _borderWidth));
        
        try
        {            
            if (imageFile != null && imageFile.exists() == true)
            {                
                this.setImage(ImageIO.read(imageFile));
                
                result = true;
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
        
        return result;
    }
    
    public void rotate()
    {
//        _originalGraphics = (Graphics2D)_originalImage.getGraphics();
//        
//        _originalGraphics.rotate(90);
//        fgdfdfgvc 
//            
//        _backgroundGraphics.drawImage(_originalImage, 0, 0, _backgroundImage.getWidth(null), _backgroundImage.getHeight(null), this);
//        
        repaint();
    }
    
    public boolean contains(int x, int y)
    {
        return super.contains(x, y);
    }
    
    
    
    @Override
    public void update(Graphics g)
    {
        g.drawImage(_backgroundImage, 0, 0, _backgroundImage.getWidth(null), _backgroundImage.getHeight(null), this);
    } 

    @Override
    public void paintComponent(Graphics g)
    {
        update(g);
    }
}
