/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phoenixstatcompiler;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;
import ScouterData.*;

public class Record 
{
    public TournamentInfo tournamentInfo = new TournamentInfo();
    public CompetitionInfo competitionInfo = new CompetitionInfo();
    public MatchInfo matchInfo = new MatchInfo();
    public SupervisorMatchInfo supervisorMatchInfo = new SupervisorMatchInfo();
    public TeamMatchInfo teamMatchInfo = new TeamMatchInfo();
    public Autonomous autonomous = new Autonomous();
    public PitScoutingInfo pitScoutingInfo = new PitScoutingInfo();
    public PitScoutingTeamInfo pitScoutingTeamInfo = new PitScoutingTeamInfo();
    public ArrayList<MatchInfo> matchInfos = new ArrayList<>();
    
    public String prsTeamName = "";
    public int prsTeamNumber = 0;
    public float prsAutoMobilityChance = 0;
    public float prsAutoToteChance = 0;
    public float prsAutoBinChance = 0;
    public float prsAutoStackChance = 0;
    public float prsTeleTotesPerRound = 0;
    public float prsTeleBinsPerRound = 0;
    public float prsTeleLitterPerRound = 0;
    public float prsTeleCoopAssistance = 0;
    public int prsTeleHighestLevel = 0;
    public int prsTeleModeLevel = 0;
    public float prsTeleMaxToteStack = 0; //1.5 for bin
    public String prsSummary = "";
    public ArrayList<Integer> prsMatches = new ArrayList<>();
    
    public ArrayList<Integer> matchMatchNumbers = new ArrayList<>();
    public ArrayList<String> matchScouterName = new ArrayList<>();  //make these into ArrayLists
    public ArrayList<Boolean> matchAutoMobility = new ArrayList<>();;
    public ArrayList<Integer> matchAutoZone = new ArrayList<>();;
    public ArrayList<Integer> matchAutoTote = new ArrayList<>();;
    public ArrayList<Integer> matchAutoBin = new ArrayList<>();;
    public ArrayList<Boolean> matchAutoStack = new ArrayList<>();;
    public ArrayList<Boolean> matchAutoInterference = new ArrayList<>();;
    public ArrayList<Boolean> matchTeleCarryTote = new ArrayList<>();;
    public ArrayList<Boolean> matchTeleCarryBin = new ArrayList<>();;
    public ArrayList<Boolean> matchTeleStackTote = new ArrayList<>();;
    public ArrayList<Boolean> matchTeleStackBin = new ArrayList<>();;
    public ArrayList<Boolean> matchTeleMobility = new ArrayList<>();;
    public ArrayList<Boolean> matchCoopAssist = new ArrayList<>();;
    public ArrayList<Integer> matchCoop1 = new ArrayList<>();;
    public ArrayList<Integer> matchCoop2 = new ArrayList<>();;
    public ArrayList<Integer> matchCoop3 = new ArrayList<>();;
    public ArrayList<Integer> matchCoop4 = new ArrayList<>();;
    public ArrayList<Integer> matchSummaryTote = new ArrayList<>();;
    public ArrayList<Integer> matchSummaryLitter = new ArrayList<>();;
    public ArrayList<Integer> matchSummaryBin = new ArrayList<>();;
    public ArrayList<String> matchComments = new ArrayList<>();
    public ArrayList<ArrayList<Stack>> matchStacks = new ArrayList<>();
    
    public float rankingAutonomousValue = 0;
    public float rankingTotesPerRound = 0;
    public float rankingBinsPerRound = 0;
    public float rankingLitterPerRound = 0;
    public float rankingCoopertitionValue = 0;
    
    public int numOfMatches = 0;
        
    public Record()
    {
         readCompetitionInfo();
    }
    
    public void readCompetitionInfo()
    {
        {
	    File file;
            BufferedReader br;  
		try
                {
			String os = System.getProperty("os.name").toLowerCase();
			String directory = null;
			if(os.contains("windows") == true){
				file = new File("C:\\Robotics\\");
				directory = "C:\\Robotics\\";
				if(!file.exists()){
					file.mkdir();
				}
			}else{
				file = new File(System.getProperty("user.home") + "/Robotics/");
				directory = System.getProperty("user.home") + "/Robotics/";
				if(!file.exists()){
					file.mkdir();
				}
			}
			file = new File(directory + "MergedData.xml");
			br = new BufferedReader(new FileReader(file));
                        String line = null;
                        String tournamentInfoFile = "";
                        while((line = br.readLine()) != null)
                        {
                            tournamentInfoFile = tournamentInfoFile.concat(line);
                        }
                        tournamentInfo.fromXML(tournamentInfoFile);
                        br.close();
                        matchInfos = tournamentInfo.competitionInfo.matches;
			System.out.println("File read");
		}
                catch(java.io.IOException e)
                {
			System.out.println(e);
		}
	}
    }
    
    public void setTeamProperties(int teamNumber)
    {
        numOfMatches = 0;
        for(int i = 0; i < matchInfos.size(); i++)
        {
            for(int j = 0; j < Constants.teamsInMatch; j++)
            {
                if(matchInfos.get(i).teamMatchInfo[j].teamNumber == teamNumber)
                {
                    numOfMatches++;
                    if(matchInfos.get(i).teamMatchInfo[j].autonomous.autoState)  //the following if blocks establish the rules for setting the autonomous value
                    {
                        rankingAutonomousValue++;
                    }
                    if(matchInfos.get(i).teamMatchInfo[j].autonomous.toteState == 1)
                    {
                        rankingAutonomousValue += .5;
                    }
                    else if(matchInfos.get(i).teamMatchInfo[j].autonomous.toteState == 2)
                    {
                        rankingAutonomousValue += 3;
                    }
                    else if(matchInfos.get(i).teamMatchInfo[j].autonomous.toteState == 3)
                    {
                        rankingAutonomousValue += 6;
                    }
                    else if(matchInfos.get(i).teamMatchInfo[j].autonomous.toteState == 4)
                    {
                        rankingAutonomousValue += 10;
                    }
                    if(matchInfos.get(i).teamMatchInfo[j].autonomous.binState == 1)
                    {
                        rankingAutonomousValue += .75;
                    }
                    else if(matchInfos.get(i).teamMatchInfo[j].autonomous.binState == 2)
                    {
                        rankingAutonomousValue += 4;
                    }
                    else if(matchInfos.get(i).teamMatchInfo[j].autonomous.binState == 3)
                    {
                        rankingAutonomousValue += 8;
                    }
                    else if(matchInfos.get(i).teamMatchInfo[j].autonomous.binState == 4)
                    {
                        rankingAutonomousValue += 13;
                    }
                    if(matchInfos.get(i).teamMatchInfo[j].autonomous.interferedAuto)
                    {
                        rankingAutonomousValue -= 2;
                    }
                    for(int k = 0; k < matchInfos.get(i).teamMatchInfo[j].stacks.size(); k++)  //the next portion of loops and if blocks calculates items per match
                    {
                        for(int l = 0; l < matchInfos.get(i).teamMatchInfo[j].stacks.get(k).stack.length; l++)
                        {
                            if(String.valueOf(matchInfos.get(i).teamMatchInfo[j].stacks.get(k).stack[l]).equals("Tote"))
                            {
                                rankingTotesPerRound++;
                            }
                            if(String.valueOf(matchInfos.get(i).teamMatchInfo[j].stacks.get(k).stack[l]).contains("Bin"))
                            {
                                rankingBinsPerRound++;
                            }
                            if(String.valueOf(matchInfos.get(i).teamMatchInfo[j].stacks.get(k).stack[l]).contains("WithLitter"))
                            {
                                rankingLitterPerRound++;
                            }
                        }
                    }
                    for(int k = 0; k < matchInfos.get(i).teamMatchInfo[j].coop.length; k++)
                    {
                        rankingCoopertitionValue += matchInfos.get(i).teamMatchInfo[j].coop[k];
                    }
                }
            }
        }
        rankingAutonomousValue = rankingAutonomousValue/numOfMatches;
        rankingTotesPerRound = rankingTotesPerRound/numOfMatches;
        rankingBinsPerRound = rankingBinsPerRound/numOfMatches;
        rankingLitterPerRound = rankingLitterPerRound/numOfMatches;
        rankingCoopertitionValue = rankingCoopertitionValue/numOfMatches;
    }
}
