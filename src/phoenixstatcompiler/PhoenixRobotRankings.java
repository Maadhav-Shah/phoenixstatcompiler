/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phoenixstatcompiler;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import javax.swing.DefaultListModel;
import javax.swing.ListModel;

/**
 *
 * @author mkshah
 */
public class PhoenixRobotRankings extends javax.swing.JFrame {

    /**
     * Creates new form PhoenixRobotRankings
     */
    
    public PhoenixRobotStats _prs;
    public Record record = new Record();
    
    public int[] teamList;
    public String[] teamNameList;
    public float[] autoList;
    public int[] autoTeamList;
    public float[] toteList;
    public int[] toteTeamList;
    public float[] binList;
    public int[] binTeamList;
    public float[] litterList;
    public int[] litterTeamList;
    public float[] coopertitionList;
    public int[] coopertitionTeamList;
    public float[] customList;
    public int[] customTeamList;
    public int[] blankTeamList;
    
    public DefaultListModel lmTeamList = new DefaultListModel();
    public DefaultListModel lmAutoList = new DefaultListModel();
    public DefaultListModel lmToteList = new DefaultListModel();
    public DefaultListModel lmBinList = new DefaultListModel();
    public DefaultListModel lmLitterList = new DefaultListModel();
    public DefaultListModel lmCoopertitionList = new DefaultListModel();
    public DefaultListModel lmCustomList = new DefaultListModel();
    
    public int tempTeamNumber = 0;
    public int index = 0;
    
    public int customPriority1 = 0;
    public int customPriority2 = 0;
    public int customPriority3 = 0;
    public int customPriority4 = 0;
    public int customPriority5 = 0;
    
    public boolean cp1Empty = false;
    public boolean cp2Empty = false;
    public boolean cp3Empty = false;
    public boolean cp4Empty = false;
    public boolean cp5Empty = false;
    
    public int[] customTeamList1;
    public int[] customTeamList2;
    public int[] customTeamList3;
    public int[] customTeamList4;
    public int[] customTeamList5;
    public int[] customTeamList6;
    public int[] customTeamList7;
    
    public int teamSelected = 0;
    
    
    public PhoenixRobotRankings() {
        initComponents();
        setPhoenixRobotRankings();
    }
    
    public void setPhoenixRobotRankings()
    {
        setTeamLists();
        organizeLists();
        sortAutoList();
        sortToteList();
        sortBinList();
        sortLitterList();
        sortCoopertitionList();
        setListModels();
        populateLists();
    }
    
    public void setTeamLists()
    {
        File file;
        BufferedReader br;  
            try
            {
                    String os = System.getProperty("os.name").toLowerCase();
                    String directory = null;
                    if(os.contains("windows") == true){
                            file = new File("C:\\Robotics\\");
                            directory = "C:\\Robotics\\";
                            if(!file.exists()){
                                    file.mkdir();
                            }
                    }else{
                            file = new File(System.getProperty("user.home") + "/Robotics/");
                            directory = System.getProperty("user.home") + "/Robotics/";
                            if(!file.exists()){
                                    file.mkdir();
                            }
                    }
                    file = new File(directory + "Davis Team List.txt");
                    br = new BufferedReader(new FileReader(file));
                    String line = null;
                    ArrayList tempTeamList = new ArrayList<String>();
                    ArrayList tempTeamNameList = new ArrayList<String>();
                    int i = 0;
                    int j = 0;
                    while((line = br.readLine()) != null)
                    {
                        j = line.indexOf(":");
                        tempTeamList.add(i, line.substring(0, j - 1));
                        tempTeamNameList.add(i, line.substring(j + 2, line.length()));
                        i++;
                    }
                    br.close();
                    teamList = new int[tempTeamList.size()];
                    teamNameList = new String[tempTeamNameList.size()];
                    autoTeamList = new int[tempTeamList.size()];
                    toteTeamList = new int[tempTeamList.size()];
                    binTeamList = new int[tempTeamList.size()];
                    litterTeamList = new int[tempTeamList.size()];
                    coopertitionTeamList = new int[tempTeamList.size()];
                    customTeamList = new int[tempTeamList.size()];
                    for(int k = 0; k < tempTeamList.size(); k++)
                    {
                        teamList[k] = Integer.valueOf(String.valueOf(tempTeamList.get(k)));
                        autoTeamList[k] = Integer.valueOf(String.valueOf(tempTeamList.get(k)));
                        toteTeamList[k] = Integer.valueOf(String.valueOf(tempTeamList.get(k)));
                        binTeamList[k] = Integer.valueOf(String.valueOf(tempTeamList.get(k)));
                        litterTeamList[k] = Integer.valueOf(String.valueOf(tempTeamList.get(k)));
                        coopertitionTeamList[k] = Integer.valueOf(String.valueOf(tempTeamList.get(k)));
                        customTeamList[k] = Integer.valueOf(String.valueOf(tempTeamList.get(k)));
                    }
                    for(int k = 0; k < tempTeamNameList.size(); k++)
                    {
                        teamNameList[k] = String.valueOf(tempTeamNameList.get(k));
                    }
                    System.out.println("File read");
            }
            catch(java.io.IOException e)
            {
                    System.out.println(e);
            }
        blankTeamList = new int[teamList.length];
        for(int i = 0; i < teamList.length; i++)
        {
            blankTeamList[i] = 0;
        }
    }
    
    public void organizeLists()
    {
        autoList = new float[autoTeamList.length];
        toteList = new float[toteTeamList.length];
        binList = new float[binTeamList.length];
        litterList = new float[litterTeamList.length];
        coopertitionList = new float[coopertitionTeamList.length];

        for(int i = 0; i < teamList.length; i++)
        {
            tempTeamNumber = teamList[i];
            record.setTeamProperties(tempTeamNumber);
            autoList[i] = record.rankingAutonomousValue;
            toteList[i] = record.rankingTotesPerRound;
            binList[i] = record.rankingBinsPerRound;
            litterList[i] = record.rankingLitterPerRound;
            coopertitionList[i] = record.rankingCoopertitionValue;
        }
    }
    
    public void sortAutoList()
    {
        float floatLarge;
        float floatTemp;
        int intTemp;
         for(int i = 0; i < autoList.length; i++)
        {
            floatLarge = autoList[i];
            index = i;
            for(int j = i + 1; j < autoList.length; j++)
            {
                if(autoList[j] > floatLarge)
                {
                    floatLarge = autoList[j];
                    index = j;
                }
            }
            floatTemp = autoList[i];
            autoList[i] = floatLarge;
            autoList[index] = floatTemp;
            intTemp = autoTeamList[i];
            autoTeamList[i] = autoTeamList[index];
            autoTeamList[index] = intTemp;
        }
    }
    
    public void sortToteList()
    {
        float floatLarge;
        float floatTemp;
        int intTemp;
         for(int i = 0; i < toteList.length; i++)
        {
            floatLarge = toteList[i];
            index = i;
            for(int j = i + 1; j < toteList.length; j++)
            {
                if(toteList[j] > floatLarge)
                {
                    floatLarge = toteList[j];
                    index = j;
                }
            }
            floatTemp = toteList[i];
            toteList[i] = floatLarge;
            toteList[index] = floatTemp;
            intTemp = toteTeamList[i];
            toteTeamList[i] = toteTeamList[index];
            toteTeamList[index] = intTemp;
        }
    }
    
    public void sortBinList()
    {
        float floatLarge;
        float floatTemp;
        int intTemp;
         for(int i = 0; i < binList.length; i++)
        {
            floatLarge = binList[i];
            index = i;
            for(int j = i + 1; j < binList.length; j++)
            {
                if(binList[j] > floatLarge)
                {
                    floatLarge = binList[j];
                    index = j;
                }
            }
            floatTemp = binList[i];
            binList[i] = floatLarge;
            binList[index] = floatTemp;
            intTemp = binTeamList[i];
            binTeamList[i] = binTeamList[index];
            binTeamList[index] = intTemp;
        }
    }
    
    public void sortLitterList()
    {
        float floatLarge;
        float floatTemp;
        int intTemp;
         for(int i = 0; i < litterList.length; i++)
        {
            floatLarge = litterList[i];
            index = i;
            for(int j = i + 1; j < litterList.length; j++)
            {
                if(litterList[j] > floatLarge)
                {
                    floatLarge = litterList[j];
                    index = j;
                }
            }
            floatTemp = litterList[i];
            litterList[i] = floatLarge;
            litterList[index] = floatTemp;
            intTemp = litterTeamList[i];
            litterTeamList[i] = litterTeamList[index];
            litterTeamList[index] = intTemp;
        }
    }
    
    public void sortCoopertitionList()
    {
        float floatLarge;
        float floatTemp;
        int intTemp;
         for(int i = 0; i < coopertitionList.length; i++)
        {
            floatLarge = coopertitionList[i];
            index = i;
            for(int j = i + 1; j < coopertitionList.length; j++)
            {
                if(coopertitionList[j] > floatLarge)
                {
                    floatLarge = coopertitionList[j];
                    index = j;
                }
            }
            floatTemp = coopertitionList[i];
            coopertitionList[i] = floatLarge;
            coopertitionList[index] = floatTemp;
            intTemp = coopertitionTeamList[i];
            coopertitionTeamList[i] = coopertitionTeamList[index];
            coopertitionTeamList[index] = intTemp;
        }
    }
    
    public void organizeCustomList()
    {
        for(int i = 0; i < customList.length; i++)
        {
            customList[i] = 0;
        }
        for(int i = 0; i < teamList.length; i++)
        {
            tempTeamNumber = customTeamList[i];
            for(int j = 0; j < teamList.length; j++)
            {
                if(customTeamList1[j] == tempTeamNumber && cp1Empty == false)
                {
                    customList[i] += ((customTeamList1.length - j) * 5);
                }
                if(customTeamList2[j] == tempTeamNumber && cp2Empty == false)
                {
                    customList[i] += ((customTeamList2.length - j) * 4);
                }
                if(customTeamList3[j] == tempTeamNumber && cp3Empty == false)
                {
                    customList[i] += ((customTeamList3.length - j) * 3);
                }
                if(customTeamList4[j] == tempTeamNumber && cp4Empty == false)
                {
                    customList[i] += ((customTeamList4.length - j) * 2);
                }
                if(customTeamList5[j] == tempTeamNumber && cp5Empty == false)
                {
                    customList[i] += ((customTeamList5.length - j) * 1);
                }
            }
        }
    }
    
    public void sortCustomList()
    {
        float floatLarge;
        float floatTemp;
        int intTemp;
        for(int i = 0; i < customList.length; i++)
        {
            floatLarge = customList[i];
            index = i;
            for(int j = i + 1; j < customList.length; j++)
            {
                if(customList[j] > floatLarge)
                {
                    floatLarge = customList[j];
                    index = j;
                }
            }
            floatTemp = customList[i];
            customList[i] = floatLarge;
            customList[index] = floatTemp;
            intTemp = customTeamList[i];
            customTeamList[i] = customTeamList[index];
            customTeamList[index] = intTemp;
        }
    }
    
    public void setListModels()
    {
        lmTeamList.setSize(teamList.length);
        lmAutoList.setSize(autoTeamList.length);
        lmToteList.setSize(toteTeamList.length);
        lmBinList.setSize(binTeamList.length);
        lmLitterList.setSize(litterTeamList.length);
        lmCoopertitionList.setSize(coopertitionTeamList.length);
        lmCustomList.setSize(customTeamList.length);
        for(int i = 0; i < teamList.length; i++)
        {
            lmTeamList.setElementAt(String.valueOf(teamList[i]), i);
            lmAutoList.setElementAt(String.valueOf(autoTeamList[i]) + ": " + String.valueOf(autoList[i]), i);
            lmToteList.setElementAt(String.valueOf(toteTeamList[i]) + ": " + String.valueOf(toteList[i]), i);
            lmBinList.setElementAt(String.valueOf(binTeamList[i]) + ": " + String.valueOf(binList[i]), i);
            lmLitterList.setElementAt(String.valueOf(litterTeamList[i]) + ": " + String.valueOf(litterList[i]), i);
            lmCoopertitionList.setElementAt(String.valueOf(coopertitionTeamList[i]) + ": " + String.valueOf(coopertitionList[i]), i);
        }    
    }
    
    public void setCustomListModel()
    {
        for(int i = 0; i < teamList.length; i++)
        {
            lmCustomList.setElementAt(String.valueOf(customTeamList[i]) + ": " + String.valueOf(customList[i]), i);
        }
    }
    
    public void populateLists()
    {
        listNumerical.setModel(lmTeamList);
        listAutonomousValue.setModel(lmAutoList);
        listTotesPerRound.setModel(lmToteList);
        listBinsPerRound.setModel(lmBinList);
        listLitterPerRound.setModel(lmLitterList);
        listCoopertitionValue.setModel(lmCoopertitionList);
    }
    
    public void populateCustomList()
    {
        listCustom.setModel(lmCustomList);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        tabLists = new javax.swing.JTabbedPane();
        pnlNumerical = new javax.swing.JPanel();
        jScrollPane8 = new javax.swing.JScrollPane();
        listNumerical = new javax.swing.JList();
        btnNumerical = new javax.swing.JButton();
        pnlAutonomousValue = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        listAutonomousValue = new javax.swing.JList();
        javax.swing.JButton btnAutonomousValue = new javax.swing.JButton();
        pnlTotesPerRound = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        listTotesPerRound = new javax.swing.JList();
        btnTotesPerRound = new javax.swing.JButton();
        pnlBinsPerRound = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        listBinsPerRound = new javax.swing.JList();
        btnBinsPerRound = new javax.swing.JButton();
        pnlLitterPerRound = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        listLitterPerRound = new javax.swing.JList();
        btnLitterPerRound = new javax.swing.JButton();
        pnlCoopertitionValue = new javax.swing.JPanel();
        jScrollPane5 = new javax.swing.JScrollPane();
        listCoopertitionValue = new javax.swing.JList();
        btnCoopertitionValue = new javax.swing.JButton();
        pnlCustom = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        comboCustom1 = new javax.swing.JComboBox();
        comboCustom2 = new javax.swing.JComboBox();
        comboCustom3 = new javax.swing.JComboBox();
        comboCustom4 = new javax.swing.JComboBox();
        comboCustom5 = new javax.swing.JComboBox();
        jScrollPane6 = new javax.swing.JScrollPane();
        listCustom = new javax.swing.JList();
        btnCustom = new javax.swing.JButton();
        btnCustomSort = new javax.swing.JButton();

        listNumerical.setFont(new java.awt.Font("Comic Sans MS", 0, 11)); // NOI18N
        listNumerical.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jScrollPane8.setViewportView(listNumerical);

        btnNumerical.setFont(new java.awt.Font("Comic Sans MS", 0, 11)); // NOI18N
        btnNumerical.setText("Display");
        btnNumerical.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNumericalActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnlNumericalLayout = new javax.swing.GroupLayout(pnlNumerical);
        pnlNumerical.setLayout(pnlNumericalLayout);
        pnlNumericalLayout.setHorizontalGroup(
            pnlNumericalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlNumericalLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlNumericalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane8, javax.swing.GroupLayout.DEFAULT_SIZE, 798, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlNumericalLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnNumerical)))
                .addContainerGap())
        );
        pnlNumericalLayout.setVerticalGroup(
            pnlNumericalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlNumericalLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnNumerical)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane8, javax.swing.GroupLayout.PREFERRED_SIZE, 378, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        tabLists.addTab("Numerical", pnlNumerical);

        listAutonomousValue.setFont(new java.awt.Font("Comic Sans MS", 0, 11)); // NOI18N
        listAutonomousValue.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jScrollPane1.setViewportView(listAutonomousValue);

        btnAutonomousValue.setFont(new java.awt.Font("Comic Sans MS", 0, 11)); // NOI18N
        btnAutonomousValue.setText("Display");
        btnAutonomousValue.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAutonomousValueActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnlAutonomousValueLayout = new javax.swing.GroupLayout(pnlAutonomousValue);
        pnlAutonomousValue.setLayout(pnlAutonomousValueLayout);
        pnlAutonomousValueLayout.setHorizontalGroup(
            pnlAutonomousValueLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlAutonomousValueLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlAutonomousValueLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 798, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlAutonomousValueLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnAutonomousValue)))
                .addContainerGap())
        );
        pnlAutonomousValueLayout.setVerticalGroup(
            pnlAutonomousValueLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlAutonomousValueLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnAutonomousValue)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 380, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        tabLists.addTab("Autonomous Value", pnlAutonomousValue);

        listTotesPerRound.setFont(new java.awt.Font("Comic Sans MS", 0, 11)); // NOI18N
        listTotesPerRound.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jScrollPane2.setViewportView(listTotesPerRound);

        btnTotesPerRound.setFont(new java.awt.Font("Comic Sans MS", 0, 11)); // NOI18N
        btnTotesPerRound.setText("Display");
        btnTotesPerRound.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTotesPerRoundActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnlTotesPerRoundLayout = new javax.swing.GroupLayout(pnlTotesPerRound);
        pnlTotesPerRound.setLayout(pnlTotesPerRoundLayout);
        pnlTotesPerRoundLayout.setHorizontalGroup(
            pnlTotesPerRoundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlTotesPerRoundLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlTotesPerRoundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 798, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlTotesPerRoundLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnTotesPerRound)))
                .addContainerGap())
        );
        pnlTotesPerRoundLayout.setVerticalGroup(
            pnlTotesPerRoundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlTotesPerRoundLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnTotesPerRound)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 380, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        tabLists.addTab("Totes Per Round", pnlTotesPerRound);

        listBinsPerRound.setFont(new java.awt.Font("Comic Sans MS", 0, 11)); // NOI18N
        listBinsPerRound.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jScrollPane3.setViewportView(listBinsPerRound);

        btnBinsPerRound.setFont(new java.awt.Font("Comic Sans MS", 0, 11)); // NOI18N
        btnBinsPerRound.setText("Display");
        btnBinsPerRound.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBinsPerRoundActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnlBinsPerRoundLayout = new javax.swing.GroupLayout(pnlBinsPerRound);
        pnlBinsPerRound.setLayout(pnlBinsPerRoundLayout);
        pnlBinsPerRoundLayout.setHorizontalGroup(
            pnlBinsPerRoundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlBinsPerRoundLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlBinsPerRoundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 798, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlBinsPerRoundLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnBinsPerRound)))
                .addContainerGap())
        );
        pnlBinsPerRoundLayout.setVerticalGroup(
            pnlBinsPerRoundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlBinsPerRoundLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnBinsPerRound)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 380, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        tabLists.addTab("Bins Per Round", pnlBinsPerRound);

        listLitterPerRound.setFont(new java.awt.Font("Comic Sans MS", 0, 11)); // NOI18N
        listLitterPerRound.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jScrollPane4.setViewportView(listLitterPerRound);

        btnLitterPerRound.setFont(new java.awt.Font("Comic Sans MS", 0, 11)); // NOI18N
        btnLitterPerRound.setText("Display");
        btnLitterPerRound.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLitterPerRoundActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnlLitterPerRoundLayout = new javax.swing.GroupLayout(pnlLitterPerRound);
        pnlLitterPerRound.setLayout(pnlLitterPerRoundLayout);
        pnlLitterPerRoundLayout.setHorizontalGroup(
            pnlLitterPerRoundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlLitterPerRoundLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlLitterPerRoundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 798, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlLitterPerRoundLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnLitterPerRound)))
                .addContainerGap())
        );
        pnlLitterPerRoundLayout.setVerticalGroup(
            pnlLitterPerRoundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlLitterPerRoundLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnLitterPerRound)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 380, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        tabLists.addTab("Litter Per Round", pnlLitterPerRound);

        listCoopertitionValue.setFont(new java.awt.Font("Comic Sans MS", 0, 11)); // NOI18N
        listCoopertitionValue.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jScrollPane5.setViewportView(listCoopertitionValue);

        btnCoopertitionValue.setFont(new java.awt.Font("Comic Sans MS", 0, 11)); // NOI18N
        btnCoopertitionValue.setText("Display");
        btnCoopertitionValue.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCoopertitionValueActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnlCoopertitionValueLayout = new javax.swing.GroupLayout(pnlCoopertitionValue);
        pnlCoopertitionValue.setLayout(pnlCoopertitionValueLayout);
        pnlCoopertitionValueLayout.setHorizontalGroup(
            pnlCoopertitionValueLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlCoopertitionValueLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlCoopertitionValueLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 798, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlCoopertitionValueLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnCoopertitionValue)))
                .addContainerGap())
        );
        pnlCoopertitionValueLayout.setVerticalGroup(
            pnlCoopertitionValueLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlCoopertitionValueLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnCoopertitionValue)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 380, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        tabLists.addTab("Coopertition Value", pnlCoopertitionValue);

        jLabel1.setFont(new java.awt.Font("Comic Sans MS", 0, 11)); // NOI18N
        jLabel1.setText("Select in order of importance:");

        comboCustom1.setFont(new java.awt.Font("Comic Sans MS", 0, 11)); // NOI18N
        comboCustom1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Priority 1", "Autonomous Value", "Totes Per Round", "Bins Per Round", "Litter Per Round", "Coopertition Value" }));

        comboCustom2.setFont(new java.awt.Font("Comic Sans MS", 0, 11)); // NOI18N
        comboCustom2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Priority 2", "Autonomous Value", "Totes Per Round", "Bins Per Round", "Litter Per Round", "Coopertition Value" }));

        comboCustom3.setFont(new java.awt.Font("Comic Sans MS", 0, 11)); // NOI18N
        comboCustom3.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Priority 3", "Autonomous Value", "Totes Per Round", "Bins Per Round", "Litter Per Round", "Coopertition Value" }));

        comboCustom4.setFont(new java.awt.Font("Comic Sans MS", 0, 11)); // NOI18N
        comboCustom4.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Priority 4", "Autonomous Value", "Totes Per Round", "Bins Per Round", "Litter Per Round", "Coopertition Value" }));

        comboCustom5.setFont(new java.awt.Font("Comic Sans MS", 0, 11)); // NOI18N
        comboCustom5.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Priority 5", "Autonomous Value", "Totes Per Round", "Bins Per Round", "Litter Per Round", "Coopertition Value" }));

        listCustom.setFont(new java.awt.Font("Comic Sans MS", 0, 11)); // NOI18N
        listCustom.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jScrollPane6.setViewportView(listCustom);

        btnCustom.setFont(new java.awt.Font("Comic Sans MS", 0, 11)); // NOI18N
        btnCustom.setText("Display");
        btnCustom.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCustomActionPerformed(evt);
            }
        });

        btnCustomSort.setFont(new java.awt.Font("Comic Sans MS", 0, 10)); // NOI18N
        btnCustomSort.setText("Sort");
        btnCustomSort.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCustomSortActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnlCustomLayout = new javax.swing.GroupLayout(pnlCustom);
        pnlCustom.setLayout(pnlCustomLayout);
        pnlCustomLayout.setHorizontalGroup(
            pnlCustomLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlCustomLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlCustomLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane6)
                    .addGroup(pnlCustomLayout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(comboCustom1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(comboCustom2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(comboCustom3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(comboCustom4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(comboCustom5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlCustomLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnCustomSort)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnCustom)))
                .addContainerGap())
        );
        pnlCustomLayout.setVerticalGroup(
            pnlCustomLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlCustomLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlCustomLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(comboCustom1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(comboCustom2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(comboCustom3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(comboCustom4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(comboCustom5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlCustomLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnCustom)
                    .addComponent(btnCustomSort))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane6, javax.swing.GroupLayout.DEFAULT_SIZE, 405, Short.MAX_VALUE)
                .addContainerGap())
        );

        tabLists.addTab("Custom", pnlCustom);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(tabLists)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(tabLists)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnNumericalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNumericalActionPerformed
        teamSelected = Integer.valueOf(String.valueOf(listNumerical.getSelectedValue()));
        String teamName = "";
        int m = 0;
        for(int i = 0; i < teamList.length; i++)
        {
            if(teamList[i] == teamSelected)
            {
                m = i;
                break;
            }
        }
        teamName = teamNameList[m];
        _prs = new PhoenixRobotStats(teamSelected, teamName);    
        _prs.setVisible(true);
    }//GEN-LAST:event_btnNumericalActionPerformed

    private void btnAutonomousValueActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAutonomousValueActionPerformed
        teamSelected = Integer.valueOf(String.valueOf(listAutonomousValue.getSelectedValue()));
        String teamName = "";
        int m = 0;
        for(int i = 0; i < teamList.length; i++)
        {
            if(teamList[i] == teamSelected)
            {
                m = i;
                break;
            }
        }
        teamName = teamNameList[m];
        _prs = new PhoenixRobotStats(teamSelected, teamName);      
        _prs.setVisible(true);
    }//GEN-LAST:event_btnAutonomousValueActionPerformed

    private void btnTotesPerRoundActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTotesPerRoundActionPerformed
        teamSelected = Integer.valueOf(String.valueOf(listTotesPerRound.getSelectedValue()));
        String teamName = "";
        int m = 0;
        for(int i = 0; i < teamList.length; i++)
        {
            if(teamList[i] == teamSelected)
            {
                m = i;
                break;
            }
        }
        teamName = teamNameList[m];
        _prs = new PhoenixRobotStats(teamSelected, teamName);        
        _prs.setVisible(true);
    }//GEN-LAST:event_btnTotesPerRoundActionPerformed

    private void btnBinsPerRoundActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBinsPerRoundActionPerformed
        teamSelected = Integer.valueOf(String.valueOf(listBinsPerRound.getSelectedValue()));
        String teamName = "";
        int m = 0;
        for(int i = 0; i < teamList.length; i++)
        {
            if(teamList[i] == teamSelected)
            {
                m = i;
                break;
            }
        }
        teamName = teamNameList[m];
        _prs = new PhoenixRobotStats(teamSelected, teamName);        
        _prs.setVisible(true);
    }//GEN-LAST:event_btnBinsPerRoundActionPerformed

    private void btnLitterPerRoundActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLitterPerRoundActionPerformed
        teamSelected = Integer.valueOf(String.valueOf(listLitterPerRound.getSelectedValue()));
        String teamName = "";
        int m = 0;
        for(int i = 0; i < teamList.length; i++)
        {
            if(teamList[i] == teamSelected)
            {
                m = i;
                break;
            }
        }
        teamName = teamNameList[m];
        _prs = new PhoenixRobotStats(teamSelected, teamName);        
        _prs.setVisible(true);
    }//GEN-LAST:event_btnLitterPerRoundActionPerformed

    private void btnCoopertitionValueActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCoopertitionValueActionPerformed
        teamSelected = Integer.valueOf(String.valueOf(listCoopertitionValue.getSelectedValue()));
        String teamName = "";
        int m = 0;
        for(int i = 0; i < teamList.length; i++)
        {
            if(teamList[i] == teamSelected)
            {
                m = i;
                break;
            }
        }
        teamName = teamNameList[m];
        _prs = new PhoenixRobotStats(teamSelected, teamName);        
        _prs.setVisible(true);
    }//GEN-LAST:event_btnCoopertitionValueActionPerformed

    private void btnCustomActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCustomActionPerformed
        int index = String.valueOf(listCustom.getSelectedValue()).indexOf(":");
        teamSelected = Integer.valueOf(String.valueOf(listCustom.getSelectedValue()).substring(0, index));
        String teamName = "";
        int m = 0;
        for(int i = 0; i < teamList.length; i++)
        {
            if(teamList[i] == teamSelected)
            {
                m = i;
                break;
            }
        }
        teamName = teamNameList[m];
        _prs = new PhoenixRobotStats(teamSelected, teamName);    
        _prs.setVisible(true);
    }//GEN-LAST:event_btnCustomActionPerformed

    private void btnCustomSortActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCustomSortActionPerformed
        customPriority1 = comboCustom1.getSelectedIndex();
        customPriority2 = comboCustom2.getSelectedIndex();
        customPriority3 = comboCustom3.getSelectedIndex();
        customPriority4 = comboCustom4.getSelectedIndex();
        customPriority5 = comboCustom5.getSelectedIndex();
        
        switch(customPriority1)
        {
            case 0: cp1Empty = true;
                    customTeamList1 = blankTeamList;
                    break;
            case 1: customTeamList1 = autoTeamList;
                    break;
            case 2: customTeamList1 = toteTeamList;
                    break;
            case 3: customTeamList1 = binTeamList;
                    break;
            case 4: customTeamList1 = litterTeamList;
                    break;
            case 5: customTeamList1 = coopertitionTeamList;
                    break;
        }
        switch(customPriority2)
        {
            case 0: cp2Empty = true;
                    customTeamList2 = blankTeamList;
                    break;
            case 1: customTeamList2 = autoTeamList;
                    break;
            case 2: customTeamList2 = toteTeamList;
                    break;
            case 3: customTeamList2 = binTeamList;
                    break;
            case 4: customTeamList2 = litterTeamList;
                    break;
            case 5: customTeamList2 = coopertitionTeamList;
                    break;
        }
        switch(customPriority3)
        {
            case 0: cp3Empty = true;
                    customTeamList3 = blankTeamList;
                    break;
            case 1: customTeamList3 = autoTeamList;
                    break;
            case 2: customTeamList3 = toteTeamList;
                    break;
            case 3: customTeamList3 = binTeamList;
                    break;
            case 4: customTeamList3 = litterTeamList;
                    break;
            case 5: customTeamList3 = coopertitionTeamList;
                    break;
        }
        switch(customPriority4)
        {
            case 0: cp4Empty = true;
                    customTeamList4 = blankTeamList;
                    break;
            case 1: customTeamList4 = autoTeamList;
                    break;
            case 2: customTeamList4 = toteTeamList;
                    break;
            case 3: customTeamList4 = binTeamList;
                    break;
            case 4: customTeamList4 = litterTeamList;
                    break;
            case 5: customTeamList4 = coopertitionTeamList;
                    break;
        }
        switch(customPriority5)
        {
            case 0: cp5Empty = true;
                    customTeamList5 = blankTeamList;
                    break;
            case 1: customTeamList5 = autoTeamList;
                    break;
            case 2: customTeamList5 = toteTeamList;
                    break;
            case 3: customTeamList5 = binTeamList;
                    break;
            case 4: customTeamList5 = litterTeamList;
                    break;
            case 5: customTeamList5 = coopertitionTeamList;
                    break;
        }
        
        organizeCustomList();
        sortCustomList();
        setCustomListModel();
        populateCustomList();
    }//GEN-LAST:event_btnCustomSortActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(PhoenixRobotRankings.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(PhoenixRobotRankings.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(PhoenixRobotRankings.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(PhoenixRobotRankings.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new PhoenixRobotRankings().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBinsPerRound;
    private javax.swing.JButton btnCoopertitionValue;
    private javax.swing.JButton btnCustom;
    private javax.swing.JButton btnCustomSort;
    private javax.swing.JButton btnLitterPerRound;
    private javax.swing.JButton btnNumerical;
    private javax.swing.JButton btnTotesPerRound;
    private javax.swing.JComboBox comboCustom1;
    private javax.swing.JComboBox comboCustom2;
    private javax.swing.JComboBox comboCustom3;
    private javax.swing.JComboBox comboCustom4;
    private javax.swing.JComboBox comboCustom5;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JScrollPane jScrollPane8;
    private javax.swing.JList listAutonomousValue;
    private javax.swing.JList listBinsPerRound;
    private javax.swing.JList listCoopertitionValue;
    private javax.swing.JList listCustom;
    private javax.swing.JList listLitterPerRound;
    private javax.swing.JList listNumerical;
    private javax.swing.JList listTotesPerRound;
    private javax.swing.JPanel pnlAutonomousValue;
    private javax.swing.JPanel pnlBinsPerRound;
    private javax.swing.JPanel pnlCoopertitionValue;
    private javax.swing.JPanel pnlCustom;
    private javax.swing.JPanel pnlLitterPerRound;
    private javax.swing.JPanel pnlNumerical;
    private javax.swing.JPanel pnlTotesPerRound;
    private javax.swing.JTabbedPane tabLists;
    // End of variables declaration//GEN-END:variables
}
